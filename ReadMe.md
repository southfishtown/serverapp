![](https://gitlab.com/southfishtown/serverapp/raw/master/Capture.PNG)

# Intro
This macOS app is the server app that receives image from the client.

# Tags
objectivec, Storyboard, image transfering, Pod

# Referenced packages
https://github.com/robbiehanson/CocoaAsyncSocket

https://github.com/6david9/ImageTransfer

https://github.com/y-natsui/CocoaAsyncSocket-Server

https://github.com/y-natsui/CocoaAsyncSocket-Client