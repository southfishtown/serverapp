//
//  GUIHandler.h
//  ServerApp
//
//  Created by Pu Zhao on 2019/10/31.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "GCDAsyncSocket.h"

@interface GUIHandler : NSObject<GCDAsyncSocketDelegate>{
}

- (IBAction)SetServerStatus:(id)sender;

-(void) StartServer;
-(void) StopServer;

@property NSString* buttonText;
@property NSString* labelText;
@property NSImage* imageToReceive;
@property bool isServerStarted;


@end
