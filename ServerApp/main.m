//
//  main.m
//  ServerApp
//
//  Created by Pu Zhao on 2019/10/31.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
