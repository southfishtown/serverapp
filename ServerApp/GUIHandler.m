//
//  GUIHandler.m
//  ServerApp
//
//  Created by Pu Zhao on 2019/10/31.
//  Copyright © 2019 Pu Zhao. All rights reserved.
//

#import "GUIHandler.h"
#define HEADER_TAG 1111
#define BODY_TAG 2222

@implementation GUIHandler{
    GCDAsyncSocket *asyncSocket;
    GCDAsyncSocket *remoteSocket;
    NSMutableArray *connectedSockets;
}

-(id) init{
    self = [super init];
    if(self){
        //Initialize properties
        self.isServerStarted = false;
        self.buttonText=@"Start Server";
        self.labelText=@"";
    }
    
    return self;
}

-(void) dealloc{
    [asyncSocket setDelegate:nil];
    [asyncSocket disconnect];
}

#pragma mark - StartServer/StopServer
//Button action for server start and server stop
 - (IBAction)SetServerStatus:(id)sender {
     if(!self.isServerStarted){
         [self StartServer];
     }else{
         [self StopServer];
     }
}

-(void) StartServer{
    //New async socket
    asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    connectedSockets = [[NSMutableArray alloc] init];
    
    if(!asyncSocket){
        self.labelText = @"Socket creation failed";
    }else{
        self.labelText = @"Socket creation successful";
        NSError *err = nil;
        uint16_t port = 20000;
        //Accept on port
        if(![asyncSocket acceptOnPort:port error:&err]){
            self.labelText = [NSString stringWithFormat:@"Socket is created successfully but port %d is not accepted", port];
        }else{
            self.labelText = [NSString stringWithFormat:@"Socket is created successfully on port %hu ", port];
            self.isServerStarted = true;
            self.buttonText = @"Stop Server";
        }
    }
}

-(void) StopServer{
    //Reset properties
    self.isServerStarted = false;
    self.buttonText = @"Start Server";
    self.labelText = @"Server Stopped";
    //Reset remoteSocket
    remoteSocket = nil;
    //Reset asyncSocket
    [asyncSocket disconnect];
}


#pragma mark - Protocol GCDAsyncSocket
-(void) socket:(GCDAsyncSocket* )sock didAcceptNewSocket:(GCDAsyncSocket*)newSocket{
    self.labelText = [NSString stringWithFormat:@"Accepted new Socket from %@:%hu", [newSocket connectedHost], [newSocket connectedPort]];
    //Assign newSocket to remoteSocket
    remoteSocket = newSocket;
    //Delay 1s to read data to prevent connection lost
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds* NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSUInteger len = sizeof(NSUInteger);
        NSMutableData *buffer = [NSMutableData data];
        //Read HEADER_TAG
        [self->remoteSocket readDataToLength:len withTimeout:-1 buffer:buffer bufferOffset:0 tag:HEADER_TAG];
    });
}

-(void) socket:(GCDAsyncSocket*) sock didReadData:(NSData*)data withTag:(long)tag{
    if(HEADER_TAG == tag){
        NSUInteger len = 0;
        [data getBytes:&len length:sizeof(NSUInteger)];
        
        NSMutableData *buffer = [NSMutableData data];
        //Read BODY_TAG
        [remoteSocket readDataToLength:len withTimeout:-1 buffer:buffer bufferOffset:0 tag:BODY_TAG];
    }
    else if(BODY_TAG == tag){
        NSImage* img = [[NSImage alloc] initWithData:data];
        self.imageToReceive = img;
        self.labelText = [NSString stringWithFormat:@"New image from client"];
    }
}

@end
